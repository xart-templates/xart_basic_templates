<?php include'_header.php'; echo "\n"; ?>
		<div class="container">
			<div class="eshop-items">
				<div class="eshop-items-title">Nadpis</div>
				<div class="item">
					<a href="">
						<figure>
							<img src="images/example/placeholder.png" alt="">
							<div class="labels">
								<span class="label label_01">New</span>
								<span class="label label_02">Akce</span>
							</div>
						</figure>
						<div class="content">
							<h2>Název produktu</h2>
							<p>Consectetur deserunt impedit nulla, eos id, sit laboriosam labore fuga, maxime provident itaque rem.</p>
						</div>
						<div class="price">
							<mark>1840,-</mark> Kč
						</div>
					</a>					
					<div class="basket">
						<form>
							<input type="number" value="1">
							<span>ks</span>
							<button type="submit">Koupit</button>
						</form>
					</div>
				</div>
			</div>
		</div>
<?php include'_footer.php'; ?>