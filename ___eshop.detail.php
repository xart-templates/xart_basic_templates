<?php include'_header.php'; echo "\n"; ?>
	<div class="container">
		<div id="content">
			<div class="eshop_detail">
				<div class="detail_image">
					<figure>
						<a href=""><img src="" alt=""></a>
					</figure>
					<ul class="thumbnails">
						<li><a href=""><img src="" alt=""></a></li>
						<li><a href=""><img src="" alt=""></a></li>
						<li><a href=""><img src="" alt=""></a></li>
					</ul>
				</div>
				<div class="detail_description">
					<div class="description">
						<h1>Lorem ipsum</h1>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex consectetur delectus, itaque temporibus minima cum, voluptatum aspernatur ipsa atque obcaecati iusto possimus, velit corrupti? Labore odit minus, laboriosam accusantium deserunt.
							<a href="" class="more">více</a>
						</p>
					</div>
					<div class="price">
						<div class="availability">
							<span class="text-success">Skladem</span>
							<span class="text-alert">Do 5ti dnů</span>
							<span class="text-error">Není skladem</span>
						</div>
						<div class="items">
							<div class="item item_01">
								<div class="label">Cena (bez DPH):</div>
								<div class="content">
									<mark>2200</mark> Kč
								</div>
							</div>
							<div class="item item_02">
								<div class="label">Cena (s DPH):</div>
								<div class="content">
									<mark>2800</mark> Kč
								</div>
							</div>
						</div>
					</div>
					<div class="options">
						<div class="items">
							<div class="item">
								<div class="label">Velikost</div>
								<div class="content">
									<select>
										<option>XL</option>
										<option>XXL</option>
									</select>
								</div>
							</div>
							<div class="item">
								<div class="label">Barva</div>
								<div class="content">
									<select>
										<option>zelená</option>
										<option>modrá</option>
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="basket">
						<input type="number" value="1" min="1">
						<span>ks</span>
						<button type="submit">Do košíku</button>
					</div>
					<div class="links">
						<ul>
							<li><a href="">Položit dotaz prodejci</a></li>
							<li><a href="">Poslat link známému</a></li>
						</ul>
					</div> 
				</div>
				<div class="mod_custom_panes">
					<div class="tabs">
						<ul>
							<li><a href="">Tab1</a></li>
							<li><a href="">Tab2</a></li>
						</ul>
					</div>
					<div class="pane">
						<p>Pane 1. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda quibusdam obcaecati earum excepturi officia placeat eum autem eius harum incidunt, aspernatur amet voluptatem repellat saepe nam dignissimos ut odit vitae.</p>
					</div>
					<div class="pane">
						<p>Pane 2. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda earum dolorem facilis explicabo adipisci quo eligendi velit aspernatur aperiam voluptatem dignissimos dolore saepe, facere soluta vitae sunt ipsa magni obcaecati.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php include'_footer.php'; ?>