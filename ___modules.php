<?php include'_header.php'; echo "\n"; ?>
		<div class="container">
			
			<!-- mod_search example -->
			<div class="mod_search">
				<div class="mod-title">Nadpis</div>
				<form>
					<div>
						<input type="text"  placeholder="Hledaný výraz">
						<button type="submit">Hledat</button>
					</div>
				</form>
			</div>


			<hr><!-- separator -->



			<!-- mod_breadcrumbs example -->
			<div class="mod_breadcrumbs">
				<ul>
					<li class="home"><a href="">Úvod</a></li>
					<li><a href="">Podstránka</a></li>
					<li>Stránka</li>
				</ul>
			</div>



			<hr><!-- separator -->



			<!-- mod_banners example -->
			<div class="mod_banners">
				<div class="mod-title">Nadpis</div>
				<ul>
					<li><a href=""><img src="images/example/placeholder.png" alt=""></a></li>
					<li><a href=""><img src="images/example/placeholder.png" alt=""></a></li>
					<li><a href=""><img src="images/example/placeholder.png" alt=""></a></li>
				</ul>
			</div>



			<hr><!-- separator -->



			<!-- mod_login example -->
			<div class="mod_login">
				<div class="mod-title">Nadpis</div>
				<form>
					<div>
						<input type="text" placeholder="Uživatelské jméno">
						<input type="password" placeholder="password">
						<button type="submit">Přihlásit</button>
					</div>
				</form>
				<div class="menu">
					<ul>
						<li><a href="">Zapomenuté heslo</a></li>
						<li><a href="">O registraci</a></li>
					</ul>
				</div>
			</div>



			<hr><!-- separator -->



			<!-- mod_menu example -->
			<div class="mod_menu">
				<div class="mod-title">Nadpis</div>
				<ul>
					<li class="act"><a href="">Lorem ipsum</a></li>
					<li><a href="">Lorem ipsum</a></li>
				</ul>
			</div>



			<hr><!-- separator -->



			<!-- mod_poll -->
			<div class="mod_poll">
				<div class="mod-title">Nadpis</div>
				<form>
					<div class="items">
						<div class="item">
							<input type="radio" name="pollid" id="poll01">
							<label for="poll01">Phasellus porta</label>
						</div>
						<div class="item">
							<input type="radio" name="pollid" id="poll02">
							<label for="poll02">Phasellus porta</label>
						</div>
					</div>
					<div class="buttons">
						<input type="submit" value="Hlasovat" />
						<a href="">Výsledky</a>
					</div>
				</form>
			</div>



			<hr><!-- separator -->



			<!-- mod_xnewsletter example -->
			<div class="mod_xnewsletter">
				<div class="mod-title">Nadpis</div>
				<form>
					<div>
						<input type="text" placeholder="Váš e-mail">
						<button type="submit">Přihlásit</button>
					</div>
				</form>
			</div>



			<hr><!-- separator -->



			<!-- mod_custom-slideshow example -->
			<div class="mod_custom-slideshow">
				<div class="items">
					<div class="item">
						<figure><img src="images/example/placeholder.png" alt=""></figure>
						<div class="mod-content">
							<h2>Lorem ipsum</h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit....</p>
							<nav>
								<a href="">Více</a>
							</nav>
						</div>
					</div>
					<div class="item">
						<figure><img src="images/example/placeholder.png" alt=""></figure>
						<div class="mod-content">
							<h2>Dolor sit amet</h2>
							<p>Cupiditate id nisi tenetur a fugit.....</p>
							<nav>
								<a href="">Více</a>
							</nav>
						</div>
					</div>
				</div>
			</div>
			<script>
				jQuery(document).ready(function($){
					// add to js/scripts.js
					$('.mod_custom-slideshow').each(function(){
						var i = $('.item',this).length;
						if(i>1){
							$(this).append('<a class="prev">&lt;</a><a class="next">&gt;</a><div class="pager"/>');
							$('.items',this).cycle({
								slides: '.item',
								fx: 'scrollHorz',
								speed: 600,
								timeout: 5000,
								pauseOnHover: true,
								swipe: true,
								prev: $('.prev',this),
								next: $('.next',this),
								pager: $('.pager',this),
							});
						}
					});
				});
			</script>



			<hr><!-- separator -->



			<!-- mod_custom-articles -->
			<div class="mod_custom-articles">
				<div class="mod-title">Nadpis</div>
				<div class="items">
					<div class="item">
						<a href="">
							<figure>
								<img src="images/example/placeholder.png" alt="">
							</figure>
							<div class="date">
								24.12.
								<mark>2016</mark>
							</div>
							<h2></h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
							<div class="nav">Více</div>
						</a>
					</div>
				</div>
				<nav>
					<a href="">Archiv</a>
				</nav>
			</div>



			<hr><!-- separator -->



			<!-- panes example -->
			<div class="panes">
				<div class="tabs">
					<ul>
						<li><a href="">Tab1</a></li>
						<li><a href="">Tab2</a></li>
					</ul>
				</div>
				<div class="pane">
					<p>Pane 1. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda quibusdam obcaecati earum excepturi officia placeat eum autem eius harum incidunt, aspernatur amet voluptatem repellat saepe nam dignissimos ut odit vitae.</p>
				</div>
				<div class="pane">
					<p>Pane 2. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda earum dolorem facilis explicabo adipisci quo eligendi velit aspernatur aperiam voluptatem dignissimos dolore saepe, facere soluta vitae sunt ipsa magni obcaecati.</p>
				</div>
			</div>
			<script>
				jQuery(document).ready(function($){
					// add to js/scripts.js
					$('.panes').each(function(){
						$(this).cycle({
							slides: '.pane',
							speed: 1,
							timeout: 0,
							pager: $('.tabs ul',this),
							pagerTemplate: '',
						});
						$('.pane',this).removeAttr('style');
					});
				});
			</script>
			<style>
				/* add to less/mod.less */
				.panes .pane {
					position: static!important;
					display: none!important;
				}
				.panes .pane.cycle-slide-active {
					display: block!important;
				}
			</style>



			<hr><!-- separator -->

		</div>
<?php include'_footer.php'; ?>