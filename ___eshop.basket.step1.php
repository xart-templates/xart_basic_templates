<?php include'_header.php'; echo "\n"; ?>
		<div class="container">
			<div id="content">
				<h1>Váš nákupní košík</h1>
				<div class="mod_menu-steps">
					<ul>
						<li class="act">
							<span>
								<mark>1</mark>
								Obsah košíku
							</span>
						</li>
						<li>
							<span>
								<mark>2</mark>
								Adresa
							</span>
						</li>
						<li>
							<span>
								<mark>3</mark>
								Doprava a platba
							</span>
						</li>
						<li>
							<span>
								<mark>4</mark>
								Rekapitulace objednávky
							</span>
						</li>
					</ul>
				</div>
				<div class="basket_items">
					<table>
						<thead>
							<tr>
								<td class="thumbnail">Náhled</td>
								<td class="title">Název zboží</td>
								<td class="price">Cena za ks</td>
								<td class="amount">Množství</td>
								<td class="sum">Cena celkem</td>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="thumbnail">
									<img src="images/example/placeholder.png" alt="">
								</td>
								<td class="title">
									<h2>Lorem ipsum</h2>
									<p>Nobis explicabo, quos, debitis quas cupiditate totam repellat itaque</p>
								</td>
								<td class="price">
									<mark>259</mark> Kč
								</td>
								<td class="amount">
									<input type="number" value="1">
									<a href="" class="remove">Smazat</a>
								</td>
								<td class="sum">
									<mark>259</mark> Kč
								</td>
							</tr>
						</tbody>
						<tfoot>
							<tr>
								<td colspan="4">Celkem s DPH</td>
								<td class="sum">
									<mark>259</mark> Kč
								</td>
							</tr>
						</tfoot>
					</table>
				</div>
				<div class="basket_voucher">
					<div class="title">Máte slevový kód?</div>
					<div class="content">
						<input type="text" placeholder="Vepište slevový kód">
					</div>
					<div class="nav">
						<a href="">Přepočítat</a>
					</div>
				</div>
				<div class="basket_navigation">
					<a href="" class="prev">Chci ještě nakupovat</a>
					<button class="next">Zadat adresu</button>
				</div>
			</div>
		</div>
<?php include'_footer.php'; ?>